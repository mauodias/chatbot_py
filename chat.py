# coding=UTF-8
#---------------------------------------------------------------------------------------------------
# Arquivo python de interação com chatbot
# Autor: Silvia Moraes
# Data: 26/03/2015
#---------------------------------------------------------------------------------------------------

import aiml
import unicodedata
from datetime import datetime

bot = aiml.Kernel()				#inicializa o interpretador
bot.loadSubs('subst.ini')			#carrega o arquivo com as substituições
bot.learn('bases.xml') 				#lê o arquivo AIML que faz referência a todas as bases do bot
bot.respond('load arquivos aiml')		#carrega todas as bases AIML do bot

bot.setBotPredicate('nome','Ricky')		#define o nome do chatbot
bot.setBotPredicate('botmaster','Turma de IA 2015/01')  #define quem são os autores do chatbot

agora = datetime.now()  #busca o tempo atual
nomeLog = "log_" + str(agora.year) + str(agora.month) + str(agora.day) + "_"+ str(agora.hour)+"h" + str(agora.minute) +"m.txt" 
arquivo = open(nomeLog,"w")  #abre o arquivo de log para gravar
arquivo.write(nomeLog+"\n")

print "\nPara encerrar a interação com o chatbot use 'exit'"  	# exit ou Ctrl+C encerram a interação
inicio = "\n"+bot.getBotPredicate('nome') + " > " + bot.respond("oi") #interação é iniciada pelo bot
print inicio
arquivo.write(inicio+"\n") #gravando a conversa

entrada = "begin"
while entrada!= "exit": 
        entrada = raw_input("Usuário > ")		#entrada do usuário
	arquivo.write("Usuário > "+ entrada+"\n")	#gravando a conversa
        entradaCodificada = entrada.decode("utf-8")     #trecho para eliminação dos acentos
        entradaNormalizada = unicodedata.normalize('NFKD',entradaCodificada).encode('ASCII', 'ignore')
        saida = bot.respond(entradaNormalizada) 	#saida é a entrada do usuário sem os acentos
	respostaBot = bot.getBotPredicate("nome")+ "> " + saida
	print respostaBot
	arquivo.write(respostaBot+"\n")			 #gravando a conversa

arquivo.close()  #fechando o arquivo



