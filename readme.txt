---------------------------
ChatBot Ricky
26/03/2015
---------------------------

------------------------------
Descrição dos arquivos
-----------------------------

O arquivo zip contém os seguintes arquivos e pastas:

- aiml : pasta que contém as bases aiml do chatbot (coloque as que você criar também nessa pasta)
- bases.xml : arquivo que contém a lista de bases aiml que o chatbot deve carregar (edite esse arquivo e inclua os nomes dos arquivos aiml que você criou. A base correspondente as universais deve permanecer por último.)
- subst.ini : arquivo de substituições: contém abreviaturas, siglas e "internetês".
- chat.py : arquivo principal escrito em python. Realiza a interação do chatbot com usuário. Retira acentuação das entradas do usuário e grava um arquivo de log.


------------------------------
Como executar
-----------------------------
É necessário primeiramente instalar o interpretador AIML para python (chamado programa Y): PyAiml
Você pode encontrar o PyAiml em http://pyaiml.sourceforge.net/

Após a instalação do interpretador PyAiml:
1) Descompacte os arquivos
2) Execute usando 
python chat.py


